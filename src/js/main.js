$(document).ready(function () {

    jQuery.getJSON("api/index.php", {
            "request": "getProductGroups"
        },
        function (response) {
            var $groupID_select = $("#groupID");

            $(response.records).each(function (key, val) {
                $groupID_select.append($("<option/>", {
                    "value": val.id,
                    "html": val.name
                }))
            })
        },
    );

    $("#productAddForm").on("submit", function (evt) {
        jQuery.getJSON("api/index.php", {
                "request": "saveProduct",
                "groupID": $("#groupID").val(),
                "name": $("#name").val()
            },
            function (data) {
                if(data.responseStatus !== undefined && data.responseStatus === "nok"){
                    $("#addDangerAlert").show(0).delay(4000).hide(0);
                    $("#addDangerAlert").html(data.message);
                }
                else{
                    $("#addSuccessAlert").html(data.message);
                    $("#addSuccessAlert").show(0).delay(4000).hide(0);
                    $("#productAddForm").get(0).reset();
                }
            });


        evt.preventDefault();
    })

});