<?php

define("__ROOT__", __DIR__);


header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/json');

session_start();

require_once __ROOT__ . DIRECTORY_SEPARATOR . "config.php";
require_once __ROOT__ . DIRECTORY_SEPARATOR . "external" . DIRECTORY_SEPARATOR . "erply" . DIRECTORY_SEPARATOR . "EAPI.class.php";

spl_autoload_register(function ($class) {
    $file = __ROOT__ . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . $class . ".class.php";
    if (file_exists($file)) {
        require_once $file;
        return true;
    }
    return false;
});

$cli = php_sapi_name() == "cli";
if ($cli) {
    (new erplywrapper())->worker();

} else {



    $response = new stdClass();
    $response->code = 200;



    $request = isset($_REQUEST["request"]) ? filter_var($_REQUEST["request"], FILTER_SANITIZE_STRING) : null;
    if (!empty($request)) {
        if (in_array($request, get_class_methods("erplywrapper"))) {
            $erplywrapper = new erplywrapper();
            $response->data = $erplywrapper->$request();
        } else {
            $response->code = 404;
        }
    } else {
        $response->code = 400;
    }


    if ($response->code == 200) {
        $result = json_encode($response->data, JSON_PRETTY_PRINT);
        echo $result;
        $output = ob_get_clean();
        echo $output;
    } else {
        http_response_code($response->code);
        exit();
    }

}