<?php

require_once __ROOT__ . DIRECTORY_SEPARATOR . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class erplywrapper
{
    private $api = null;
    private $channel = null;

    function __construct()
    {

    }

    private function eapiInit()
    {
        if ($this->api !== null) {
            return;
        }
        $this->api = new EAPI();
        $this->api->clientCode = config::$erply_client_code;
        $this->api->username = config::$erply_username;
        $this->api->password = config::$erply_password;
        $this->api->url = "https://" . $this->api->clientCode . ".erply.com/api/";
    }
    private function rabbitInit()
    {
        $connection = new AMQPStreamConnection(config::$rabbitmq_host, 5672, config::$rabbitmq_username, config::$rabbitmq_password);
        $this->channel = $connection->channel();
        $this->channel->queue_declare(config::$rabbitmq_queue, false, false, false, false);
    }

    public function getProductGroups()
    {
        return $this->call();
    }

    public function saveProduct()
    {
        $newProdcutParams = array();
        $newProdcutParams["groupID"] = filter_var($_REQUEST["groupID"], FILTER_SANITIZE_NUMBER_INT);
        $newProdcutParams["name"] = filter_var($_REQUEST["name"], FILTER_SANITIZE_STRING);

        $searchExistingProduct = $this->getProducts(array("name" => $newProdcutParams["name"]));

        if ($searchExistingProduct["status"]["responseStatus"] === "ok") {
            if (count($searchExistingProduct["records"]) === 0) {
                $result = new stdClass();
                $result->responseStatus = "ok";
                if (config::$use_message_broker) {
                    $this->rabbitCall("saveProduct", $newProdcutParams);
                    $result->message = "Product queued";
                } else {
                    $this->eapiCall("saveProduct", $newProdcutParams);
                    $result->message = "Product created";
                }
                return $result;
            } else {
                $result = new stdClass();
                $result->responseStatus = "nok";
                $result->message = "Duplicate product";
                return $result;
            }
        } else {
            $result = new stdClass();
            $result->responseStatus = "nok";
            $result->message = "error occurred while searching for existing products";
            return $result;
        }
    }

    public function getProducts($params = array())
    {
        return $this->call($params);
    }


    private function call($params = array(), $request = null)
    {
        if ($request === null) {
            $request = debug_backtrace()[1]['function'];
        }
        return $this->eapiCall($request, $params);
    }

    private function rabbitCall($reqeust, $params)
    {
        if ($this->channel === null) {
            $this->rabbitInit();
        }
        $msgContents = new stdClass();
        $msgContents->request = $reqeust;
        $msgContents->params = $params;
        $msgContents->received = date("Y-m-d H:i:s");
        $msg = new AMQPMessage(json_encode($msgContents));
        $this->channel->basic_publish($msg, '', config::$rabbitmq_queue);

        $result = new stdClass();
        $result->responseStatus = "ok";
        return $result;
    }


    private function eapiCall($request, $params)
    {
        $this->eapiInit();
        $result = $this->api->sendRequest($request, $params);
        $output = json_decode($result, true);
        return $output;
    }

    public function worker()
    {
        if ($this->channel === null) {
            $this->rabbitInit();
        }
        $this->channel->basic_consume(config::$rabbitmq_queue, '', false, true, false, false, array($this, "onMessage"));
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }
    function onMessage($msg)
    {
        $request = json_decode($msg->body);
        if($request->request == "saveProduct"){
            $result = $this->eapiCall($request->request, (array)$request->params);
            if($result["status"]["responseStatus"]==="ok"){
                echo "Product '{$request->params->name}' added\n";
            }
        }
    }
}
