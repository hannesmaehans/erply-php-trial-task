# Erply PHP trial-task

## Installing php-amqplib
```
cd src/api/
composer install
```

## Running project


### 1 Running RabbitMQ in docker
```
docker run -p 5671:5671 -p 5672:5672 -d --hostname my-rabbit --name some-rabbit rabbitmq:3
```

### 2 Running webserver
```
cd src/
php -S localhost:1234
```

### 3 Running background service
```
cd src/
php index.php
```

## Testing

Navigate to http://localhost:1234/

1) Choose product group
2) Type new product name
3) Submit
